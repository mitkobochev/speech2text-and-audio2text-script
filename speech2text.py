import speech_recognition as sr
import pyttsx3

def speech_to_text():
    r = sr.Recognizer()

    def speakText(text):

        my_voice = pyttsx3.init()
        my_voice.say(text)
        my_voice.runAndWait()


    while(1):
        try:
            with sr.Microphone() as mic:

                r.adjust_for_ambient_noise(mic, duration=0.2)
                print("Speak now please...")

                my_audio = r.listen(mic)
                try:
                    my_recognized_text = r.recognize_google(my_audio, language="bg-BG")
                except:
                    my_recognized_text = r.recognize_google(my_audio, language="en-EN")
              

                print("Did you say:", my_recognized_text)
                speakText(my_recognized_text)
                break
            
        except sr.RequestError as err:
            print("Could not request results; {0}".format(err))

        except sr.UnknownValueError:
            print("Unknown error occurred")