import audio2Text
import speech2text

print("Hello to Speech2Text and Audio2Text script!")
print("Please press 1 for Speech transcript or press 2 for Audio file transcript")
num = input()

def main():
    if num == "1":
        return speech2text.speech_to_text()
    elif num == "2":
        return audio2Text.my_audio_file()
    else:
        print("No such command found! Please try again!")

main()