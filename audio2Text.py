import speech_recognition as sr

def my_audio_file():
    print("Please enter your .wav file name!")
    my_audio_file = input()
    try:
        r = sr.Recognizer()
        with sr.AudioFile(my_audio_file) as source:
            audio_data = r.record(source)
            text = r.recognize_google(audio_data)
            print(text)

    except:
        print("Oops something went wrong! Your file may not exist or the name is not correct!")
